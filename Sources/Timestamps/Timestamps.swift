import Foundation

public class Timestamp {

  public let value: Int

  public var seconds: Int {
    return value
  }

  public var string: String {
    return String(value)
  }

  public init() {
    value = Int(Date().timeIntervalSince1970)
  }
}
