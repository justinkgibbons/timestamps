import XCTest

import TimestampsTests

var tests = [XCTestCaseEntry]()
tests += TimestampsTests.allTests()
XCTMain(tests)